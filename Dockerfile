FROM openjdk:8 as ukr

ENV JAVA_TOOL_OPTIONS -Dfile.encoding=UTF8

RUN git clone https://github.com/brown-uk/dict_uk.git
RUN rm ./dict_uk/distr/hunspell/src/main/groovy/Hunspell.groovy
COPY ./Hunspell.groovy ./dict_uk/distr/hunspell/src/main/groovy

WORKDIR ./dict_uk/distr/hunspell
RUN ../../gradlew hunspell

FROM postgis/postgis:11-2.5-alpine

# set work directory
WORKDIR /web/app

USER postgres
COPY ./docker-entrypoint-initdb.d/ukrainian.sh /docker-entrypoint-initdb.d
COPY ./tsearch_data/ukrainian.syn /usr/local/share/postgresql/tsearch_data/ukrainian.syn
COPY --from=ukr  /dict_uk/distr/hunspell/build/hunspell/uk_UA.dic /usr/local/share/postgresql/tsearch_data/ukrainian.dict
COPY --from=ukr  /dict_uk/distr/hunspell/build/hunspell/uk_UA.aff /usr/local/share/postgresql/tsearch_data/ukrainian.affix
COPY --from=ukr  /dict_uk/distr/postgresql/ukrainian.stop /usr/local/share/postgresql/tsearch_data/ukrainian.stop
