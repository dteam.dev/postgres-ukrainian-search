# Postgres Ukrainian Search

This repo contains container for PostgreSQL with enabled ukrainian search dictionaries.

If you need to add new synonims, look into [CONTRIBUTION.md](https://gitlab.com/dteam.dev/postgres-ukrainian-search/-/blob/master/CONTRIBUTING.md)

